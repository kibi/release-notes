# Tapio Lehtonen <tale@debian.org>, 2005, 2007, 2009.
# Tommi Vainikainen <thv@iki.fi>, 2006-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes\n"
"POT-Creation-Date: 2021-05-04 19:18+0200\n"
"PO-Revision-Date: 2009-09-17 11:23+0300\n"
"Last-Translator: Tapio Lehtonen <tale@debian.org>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "fi"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
#, fuzzy
#| msgid "Managing your &oldreleasename; system"
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "&oldreleasename;-järjestelmän hallinnointi"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Tässä liitteessä neuvotaan miten varmasti saadaan asennettua tai päivitettyä "
"julkaisun &oldreleasename; paketteja ennen päivitystä julkaisuun "
"&releasename;. Tämän pitäisi olla tarpeellista vain erikoistilanteissa."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "&oldreleasename;-järjestelmän päivitys"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
#, fuzzy
#| msgid ""
#| "Basically this is no different than any other upgrade of &oldreleasename; "
#| "you've been doing.  The only difference is that you first need to make "
#| "sure your package list still contains references to &oldreleasename; as "
#| "explained in <xref linkend=\"old-sources\"/>."
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Periaatteeltaan tämä ei eroa muista tekemistäsi julkaisun &oldreleasename; "
"päivityksistä. Ainoa ero on että ensin on varmistuttava pakettiluettelon yhä "
"viittaavan julkaisun &oldreleasename; paketteihin kuten selitetään kohdassa "
"<xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Jos päivität järjestelmän Debianin asennuspalvelinta käyttäen, se "
"päivitetään automaattisesti viimeisimpään julkaisun &oldreleasename; "
"korjauspäivitykseen."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
#, fuzzy
#| msgid "Checking your sources list"
msgid "Checking your APT source-list files"
msgstr "Asennuspalvelimien luettelon tarkistaminen"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
#, fuzzy
#| msgid ""
#| "If any of the lines in your <filename>/etc/apt/sources.list</filename> "
#| "refer to 'stable', you are effectively already <quote>using</quote> "
#| "&releasename;.  If you have already run <literal>apt-get update</"
#| "literal>, you can still get back without problems following the procedure "
#| "below."
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Jos tiedostossa <filename>/etc/apt/sources.list</filename> on rivejä jotka "
"viittaavat jakeluun \"stable\", on julkaisu &releasename; käytännössä jo "
"käytössä. Jos komento <literal>apt-get update</literal> on jo suoritettu, "
"voidaan palata vanhaan tilanteeseen vaivattomasti seuraavassa selitettävällä "
"tavalla."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Jos julkaisusta &releasename; on jo asennettu paketteja, ei luultavasti ole "
"järkeä asentaa enää paketteja julkaisusta  &oldreleasename;. Tässä "
"tapauksessa on tehtävä päätös jatketaanko vai ei. Pakettien palauttaminen "
"vanhaan versioon on mahdollista, mutta sitä ei käsitellä tässä."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
#, fuzzy
#| msgid ""
#| "Open the file <filename>/etc/apt/sources.list</filename> with your "
#| "favorite editor (as <literal>root</literal>) and check all lines "
#| "beginning with <literal>deb http:</literal> or <literal>deb ftp:</"
#| "literal> for a reference to <quote><literal>stable</literal></quote>.  If "
#| "you find any, change <literal>stable</literal> to "
#| "<literal>&oldreleasename;</literal>."
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Muokkaa tiedostoa <filename>/etc/apt/sources.list</filename> "
"mieliteksturillasi (käyttäjänä <literal>root</literal>) ja tarkista kaikilta "
"riveiltä joiden alussa on <literal>deb http:</literal> tai <literal>deb ftp:"
"</literal> onko niillä viittaus jakeluun \"<literal>stable</literal>\". Jos "
"on, vaihda <literal>stable</literal> tilalle <literal>&oldreleasename;</"
"literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
#, fuzzy
#| msgid ""
#| "If you have any lines starting with <literal>deb file:</literal>, you "
#| "will have to check for yourself if the location they refer to contains an "
#| "&oldreleasename; or a &releasename; archive."
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Jos on rivejä jotka alkavat <literal>deb file:</literal>, on sinun "
"tarkistettava onko niiden osoittamassa paikassa julkaisun &oldreleasename; "
"vai julkaisun &releasename; varastoalue."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
#, fuzzy
#| msgid ""
#| "Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
#| "Doing so would invalidate the line and you would have to run <command>apt-"
#| "cdrom</command> again.  Do not be alarmed if a 'cdrom' source line refers "
#| "to <quote><literal>unstable</literal></quote>.  Although confusing, this "
#| "is normal."
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Älä muuta mitään riviä jonka alussa on <literal>deb cdrom:</literal>. Rivin "
"muuttaminen tekee siitä kelvottoman ja komento <prgn>apt-cdrom</prgn> on "
"suoritettava uudelleen. Älä huoli jos \"cdrom\"-rivillä viitataan jakeluun "
"\"<literal>unstable</literal>\". Tämä on normaalia vaikkakin hämäävää."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Mikäli olet tehnyt muutoksia, tallenna tiedosto ja suorita"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, fuzzy, no-wrap
#| msgid "# apt-get update\n"
msgid "# apt update\n"
msgstr "# apt-get update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "pakettiluettelon päivittämiseksi."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
